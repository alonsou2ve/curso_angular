
import {v4 as uuid} from 'uuid';

export class DestinoViaje {
    selected: boolean;
    id = uuid();
    servicios: string[];
    public votes: number = 0;
    constructor(public nombre:string , imagenurl:string) { 
        this.servicios = ['pileta','desayuno'];
    }
    
    setSelected(s: boolean) {
        this.selected = s;
    }
    isSelected() {
      return this.selected;
    }

    voteUp(): any {
        this.votes++;
      }
      voteDown(): any {
        this.votes--;
      }
    
}


