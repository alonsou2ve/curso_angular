import { Component, OnInit, EventEmitter, Output } from '@angular/core';
import {DestinoViaje} from './../../models/destino-viaje.models';
import { DestinosApiClient } from './../../models/destinos-api-client.models';
import {Store} from '@ngrx/store';
import {AppState} from '../../app.module';
import { ElegidoFavoritoAction, NuevoDestinoAction } from './../../models/destinos-viajes-state.model';


@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
  providers: [DestinosApiClient]
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  update: string[];
  all;
  
   
  constructor(public destinosApiClient:DestinosApiClient, private store: Store<AppState>) {
    this.onItemAdded = new EventEmitter();
    this.update = [];
    this.store.select(state=>state.destinos.favorito)
    .subscribe(d =>{
        const fav = d;
        if (d != null) {
          this.update.push('Se ha elegido a' + d.nombre);
        }
    });
    store.select(state => state.destinos.items).subscribe(items => this.all = items);
  }

  ngOnInit() {
  }

 agregado(d: DestinoViaje) {
   this.destinosApiClient.add(d);
   this.onItemAdded.emit(d);
 }

  elegido(e: DestinoViaje) {
    this.destinosApiClient.elegir(e);
  }

  borrado(e: DestinoViaje) {
     this.destinosApiClient.borrar(e);
  }

  getAll() {

  }
}
